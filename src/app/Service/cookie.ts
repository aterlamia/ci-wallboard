import {Injectable, Optional} from '@angular/core';

@Injectable()
export class CookieService implements ICookieService {
    protected get cookieString(): string {
        return document.cookie || '';
    }

    protected set cookieString(val: string) {
        document.cookie = val;
    }

    /**
     * @name CookieService#get
     *
     * @description
     * Returns the value of given cookie key.
     *
     * @param {string} key Id to use for lookup.
     * @returns {string} Raw cookie value.
     */
    get(key: string): string {
        return (<any>this._cookieReader())[key];
    }

    /**
     * @name CookieService#put
     *
     * @description
     * Sets a value for given cookie key.
     *
     * @param {string} key Id for the `value`.
     * @param {string} value Raw value to be stored.
     * @param {string} path
     * @param {date} expire
     */
    put(key: string, value: string, path: string, expire: Date) {
        this._cookieWriter()(key, value, path, expire);
    }

    private _cookieReader(): Object {
        let lastCookies = {};
        let lastCookieString = '';
        let that = this;

        let cookieArray: string[], cookie: string, i: number, index: number, name: string;
        let currentCookieString = this.cookieString;
        if (currentCookieString !== lastCookieString) {
            lastCookieString = currentCookieString;
            cookieArray = lastCookieString.split('; ');
            lastCookies = {};
            for (i = 0; i < cookieArray.length; i++) {
                cookie = cookieArray[i];
                index = cookie.indexOf('=');
                if (index > 0) {  // ignore nameless cookies
                    name = that._safeDecodeURIComponent(cookie.substring(0, index));
                    // the first value that is seen for a cookie is the most
                    // specific one.  values for the same cookie name that
                    // follow are for less specific paths.
                    if (this.isBlank((<any>lastCookies)[name])) {
                        (<any>lastCookies)[name] = that._safeDecodeURIComponent(cookie.substring(index + 1));
                    }
                }
            }
        }
        return lastCookies;
    }

    private _cookieWriter() {
        let that = this;

        return function (name: string, value: string, path: string, expire: Date) {
            that.cookieString = that._buildCookieString(name, value, path, expire);
        };
    }

    private _safeDecodeURIComponent(str: string) {
        try {
            return decodeURIComponent(str);
        } catch (e) {
            return str;
        }
    }

    private _buildCookieString(name: string, value: string, path: string, expire: Date): string {
        let str = encodeURIComponent(name) + '=' + encodeURIComponent(value);
        str += ';path=' + path;
        str += ';expires=' + expire.toUTCString();
        str += ';secure';

        // per http://www.ietf.org/rfc/rfc2109.txt browser must allow at minimum:
        // - 300 cookies
        // - 20 cookies per unique domain
        // - 4096 bytes per cookie
        let cookieLength = str.length + 1;
        if (cookieLength > 4096) {
            console.log(`Cookie \'${name}\' possibly not set or overflowed because it was too 
      large (${cookieLength} > 4096 bytes)!`);
        }
        return str;
    }

    private isBlank(obj: any): boolean {
        return obj === undefined || obj === null;
    }

    private isPresent(obj: any): boolean {
        return obj !== undefined && obj !== null;
    }

    private isString(obj: any): obj is string {
        return typeof obj === 'string';
    }
}

export interface ICookieService {
    get(key: string): string;

    put(key: string, value: string, path: string, expire: Date): void;
}