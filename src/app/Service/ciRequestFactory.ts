import {Http} from '@angular/http';
import {Settings} from "./settings";
import {BambooRequest} from "./bambooRequest";
import {TravisRequest} from "./travisRequest";
import {AbstractCiRequest} from "./abstractCiRequest";
import {Injectable} from "@angular/core";
import {GitlabRequest} from "./gitlabRequest";

@Injectable()
export class CiRequestFactory {
    private http: Http;
    private settings: Settings;

    constructor(http: Http, settings: Settings) {
        this.http = http;
        this.settings = settings;
    }

    create(type: String): AbstractCiRequest {
        switch (type) {
            case "bamboo":
                return new BambooRequest(this.http, this.settings);
            case "gitlab":
                return new GitlabRequest(this.http, this.settings);
            case "travis":
            default:
                return new TravisRequest(this.http, this.settings);
        }
    }
}
