'use strict';
import {Pipe, PipeTransform} from '@angular/core';
import {Store} from "../Service/store";
import {Md5} from 'ts-md5/dist/md5';
import {CookieService} from '../Service/cookie';

@Pipe({name: 'gravatar'})
export class Gravatar implements PipeTransform {

    transform(email, args) {
        let store = new Store(new CookieService());
        let settings = store.getValue('user-settings');

        if (settings !== null) {
            for (let i = 0; i < settings.length; i++) {
                let setting = settings[i];
                if (setting.name === email) {
                    return setting.img;
                }
            }
        }
        let baseUrl = 'http://www.gravatar.com/avatar/';
        let hash = Md5.hashStr(email.trim().toLowerCase(), false);

        return baseUrl + hash;
    }
}
