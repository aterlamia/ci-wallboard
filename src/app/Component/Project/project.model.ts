'use strict';

export class Project {
    private _id: number;
    private _active: boolean;
    private _slug: string;
    private _description: string;
    private _lastBuildStartedAt: string;
    private _lastBuildFinishedAt: string;
    private _lastBuildId: number;
    private _lastBuildNr: number;
    private _lastBuildState: string;

    constructor(id: number, slug: string, active: boolean, description: string, lastBuildStartedAt: string, lastBuildFinishedAt: string, lastBuildId: number, lastBuildNr: number, lastBuildState: string) {
        this._id = id;
        this._active = active;
        this._slug = slug;
        this._description = description;
        this._lastBuildStartedAt = lastBuildStartedAt;
        this._lastBuildFinishedAt = lastBuildFinishedAt;
        this._lastBuildId = lastBuildId;
        this._lastBuildNr = lastBuildNr;
        this._lastBuildState = lastBuildState;
    }

    getId(): number {
        return this._id;
    }


    getActive(): boolean {
        return this._active;
    }

    getSlug(): string {
        return this._slug;
    }

    getDescription(): string {
        return this._description;
    }

    getLastBuildStartedAt(): string {
        return this._lastBuildStartedAt;
    }

    getLastBuildFinishedAt(): string {
        return this._lastBuildFinishedAt;
    }

    getLastBuildId(): number {
        return this._lastBuildId;
    }


    setLastBuildId(id:number): void {
         this._lastBuildId = id;
    }


    getLastBuildNr(): number {
        return this._lastBuildNr;
    }

    getLastBuildState(): string {
        return this._lastBuildState;
    }

    equals(project: Project): boolean {
        return (
            this.getId() === project.getId() &&
            this.getActive() === project.getActive() &&
            this.getSlug() === project.getSlug() &&
            this.getLastBuildState() === project.getLastBuildState() &&
            this.getLastBuildStartedAt() === project.getLastBuildStartedAt() &&
            this.getLastBuildFinishedAt() === project.getLastBuildFinishedAt() &&
            this.getLastBuildNr() === project.getLastBuildNr() &&
            this.getLastBuildId() === project.getLastBuildId() &&
            this.getDescription() === project.getDescription()
        );
    }
}
