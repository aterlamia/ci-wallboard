'use strict';

import {Component, Output, Input} from '@angular/core';

// COMPONENTS
import {StatusSuccessComponent} from './status.success.component';

@Component({
    selector: 'failed-item',
    templateUrl: './status.failed.html'
})

export class StatusFailedComponent extends StatusSuccessComponent {
    @Input()
    build;
    pos;

}
