'use strict';

import {CiRequestFactory} from "../../Service/ciRequestFactory";
import {RepoSettings} from '../Setting/reposettings.model';
import {Injectable} from "@angular/core";

@Injectable()
export class ReposService {
    repos = {};
    previousRepos = {};
    retrievedBuilds = {};
    pendingProjects = [];
    private requestFactory: CiRequestFactory;

    constructor(requestFactory: CiRequestFactory) {
        this.requestFactory = requestFactory;
    }

    public clear() {
      this.repos = {};
      this.previousRepos = {};
      this.retrievedBuilds = {};
      this.pendingProjects = [];
    }

    updateBuild(project, build, slug, key) {
        if (build.length === 0) {
            return;
        }
        this.retrievedBuilds[slug] = {};
        this.retrievedBuilds[slug].id = build.getId();
        this.retrievedBuilds[slug].type = build.getType();

        if (build.getType() === "push") {
            this.pendingProjects[key] = this.pendingProjects[key] || [];
            this.pendingProjects[key].push(project);
        }
    }

    getPending(builds, repoSettings: RepoSettings) {
        let selfBuilds = builds;
        let repo = this.repos[repoSettings.getKey()];
        console.info('REPO SERVIVE -- getting pending for ' + repoSettings.getName());
        Object.keys(repo).forEach((rkey) => {
                if (typeof(builds[rkey]) !== 'undefined') {
                    let project = repo[rkey];
                    let build = selfBuilds[rkey];

                    if (
                        project.getLastBuildState() !== build.getState()
                        && (typeof(this.retrievedBuilds[project.getSlug()]) === "undefined"
                            || this.retrievedBuilds[project.getSlug()].id !== project.getLastBuildId()
                        )
                    ) {
                        let request = this.requestFactory.create(repoSettings.getEnvironment());
                        console.info('get build');
                        console.info(project);

                        let resource = request.getBuild(repoSettings, project.getSlug(), project.getId(), project.getLastBuildId());

                        resource.then(
                            build => this.updateBuild(project, build, project.getSlug(), repoSettings.getKey()),
                            error => console.error(error)
                        );

                        console.info('REPO SERVCE -- PENDING: different states for' + project.getSlug() + ' on ' + repoSettings.getKey());
                        console.info('REPO SERVCE -- PENDING: state project = ' + project.getLastBuildState() + ' for build id ' + project.getLastBuildId() + ' on ' + repoSettings.getKey());
                        console.info('REPO SERVCE -- PENDING: state build = ' + build.getState() + ' for build id ' + build.getId() + ' on ' + repoSettings.getKey());
                    }
                }
            }, this
        );
    }

    getProjectsToUpdate(repoSettings: RepoSettings, projects) {
        let that = this;
        let repoKey = repoSettings.getKey();

        projects = this.toObject(projects);
        if (typeof(this.repos[repoKey]) === "undefined") {
            this.repos[repoKey] = projects;
        } else {
            this.previousRepos = this.clone(this.repos);
            this.repos[repoKey] = this.mergeObjects(this.repos[repoKey], projects);
        }

        let updatableProjects = this.pendingProjects[repoKey] || [];

        // Reset pendingProjects.
        this.pendingProjects[repoKey] = [];

        if (typeof this.previousRepos[repoKey] === "undefined") {
            console.info('REPO SERVICE -- UPDATE: empty list so returning all for ' + repoKey);
            return this.repos[repoKey];
        }

        Object.keys(this.repos[repoKey]).forEach((key) => {
            let project = that.repos[repoKey][key];
            if (
                typeof(that.previousRepos[repoKey][project.getId()]) === "undefined"
                || project.equals(that.previousRepos[repoKey][project.getId()]) === false
            ) {
                console.info('REPO SERVICE: difference for project ' + project.getSlug() + ' for envirionment ' + repoSettings.getEnvironment());
                updatableProjects.push(project);
            }
        });
        return updatableProjects;
    }

    /**
     * Create a associative array for projects.
     *
     * @param projects
     * @returns {{}}
     */
    toObject(projects) {
        let repos = {};
        for (let i = 0; i < projects.length; i++) {
            let project = projects[i];
            repos[project.getId()] = project;
        }
        return repos;
    }

    /**
     * Merge the attributes of two objects.
     *
     * @param obj1
     * @param obj2
     * @returns {{}}
     */
    mergeObjects(obj1, obj2) {
        var obj3 = {};
        for (var attrname1 in obj1) {
            obj3[attrname1] = obj1[attrname1];
        }
        for (var attrname2 in obj2) {
            obj3[attrname2] = obj2[attrname2];
        }
        return obj3;
    }

    clone(orig) {
        let origProto = Object.getPrototypeOf(orig);
        return Object.assign(Object.create(origProto), orig);
    }

    getRepos() {
        return this.repos;
    }
}
