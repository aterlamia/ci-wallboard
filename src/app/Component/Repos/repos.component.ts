'use strict';
declare let $: any;

import {Component, ChangeDetectorRef} from '@angular/core';
import {Store} from "../../Service/store";
import {CiRequestFactory} from "../../Service/ciRequestFactory";
import {RepoSettings} from '../Setting/reposettings.model';
import {ReposService} from './repos.service';

import {ArraySortPipe} from '../../Pipes/arrayfilter';

import {OnDestroy, OnInit} from '@angular/core';
import {Project} from "../Project/project.model";
import {AbstractCiRequest} from "../../Service/abstractCiRequest";

@Component({
  templateUrl: './repos.component.html'
})


export class ReposComponent implements OnDestroy, OnInit {
  public builds: {} = {};
  public successBuildsArray = [];
  public failedBuildsArray = [];
  public pendingBuildsArray = [];
  private timer;
  private interval: number = 1000;
  private repoSettings: Array<RepoSettings>;
  private store: Store;
  private requestFactory: CiRequestFactory;
  private reposService: ReposService;
  private cdr: ChangeDetectorRef;
  private errors: number;
  private loadedSettings: any = {};

  constructor(store: Store, requestFactory: CiRequestFactory, reposService: ReposService, cdr: ChangeDetectorRef) {
    this.store = store;
    this.requestFactory = requestFactory;
    this.reposService = reposService;
    this.cdr = cdr;
    this.repoSettings = this.store.getRepoSettings();
  }

  /**
   * Initializes builds and repos.
   */
  init(first: boolean) {
    let that = this;
    this.repoSettings.forEach(parseRepositories);

    let settings = this.store.getValue('project-settings');

    settings.forEach((project) => {
      project.projectSettings.forEach((setting) => {
        this.loadedSettings[project.name + '_' + setting.name] = setting.enabled;
      });
    });

    function parseRepositories(repoSetting) {
      let shouldUpdate = repoSetting.shouldUpdate(that.interval);
      if (first || (!that.isEmpty(repoSetting.getName()) && shouldUpdate)) {
        let request = that.requestFactory.create(repoSetting.getEnvironment());
        let settings: RepoSettings = repoSetting;

        let resource = request.loadProjectData(repoSetting);
        resource.then(
          project => {
            that.checkBuildsForProject(project, settings);
          },
          error => {
            console.error(error);
          },
        );
      }
    }
  }

  /**
   * Get the builds for a given Project.
   *
   * @param projects
   * @param {RepoSettings} repoSettings
   */
  checkBuildsForProject(projects, repoSettings: RepoSettings) {
    let updateableProjects = this.reposService.getProjectsToUpdate(repoSettings, projects);
    this.updatePending(repoSettings);

    this.loadBuildsForProjects(updateableProjects, repoSettings);
  }

  /**
   * Loads builds for a given repository and sets this to the scope.
   *
   * @param projects
   * @param {RepoSettings} userSettings
   */
  loadBuildsForProjects(projects: Array<Project>, userSettings: RepoSettings) {
    let that = this;
    Object.keys(projects).forEach(parseProjects);

    function parseProjects(key: string) {
      let project: Project = projects[key];
      if (that.loadedSettings[userSettings.getKey() + '_' + project.getSlug()] === false) {
        return;
      }
      let slug: string = project.getSlug().replace(userSettings.getName() + '/', "");
      let request: AbstractCiRequest = that.requestFactory.create(userSettings.getEnvironment());
      let resource = request.getBuilds(userSettings, slug, project.getId(), false);
      resource.then(
        build => {
          if (project.getLastBuildId() === 0) {
            project.setLastBuildId(build.id);
          }
          that.updateBuild(project, build, slug);
        },
        error => {
          console.error(error);
        },
      );
    }
  };

  ngOnInit() {
    this.init(true);
    this.timer = setInterval(() => {
      this.init(false);
      this.adjustToScreenHeight()

    }, this.interval);

    console.info('REPO COMPONENT: Added interval');
  }

  ngOnDestroy() {
    clearInterval(this.timer);
    this.repoSettings = null;
    this.builds = {};
    this.reposService.clear();
    console.info('REPO COMPONENT: Removed interval');
  }

  isEmpty(val) {
    return (val === "" || typeof(val) === "undefined" || val === null);
  }

  updateBuild(repo, builds, slug) {
    console.info('REPOS COMPONENT -- Updating  builds for ' + slug);
    if (typeof(builds) !== 'undefined' && typeof(builds[0]) !== 'undefined') {
      let newbuild = builds[0];
      newbuild.name = slug;
      this.builds[repo.getId()] = newbuild;
      this.errors = 0;
    }
    this.pendingBuildsArray = [];
    this.failedBuildsArray = [];
    this.successBuildsArray = [];

    Object.keys(this.builds).forEach((build) => {
      if (this.builds[build].isPassing()) {
        this.successBuildsArray.push({key: build, val: this.builds[build]})
      } else if (this.builds[build].isFailed()) {
        this.failedBuildsArray.push({key: build, val: this.builds[build]})
      } else if (this.builds[build].isBuilding()) {
        this.pendingBuildsArray.push({key: build, val: this.builds[build]})
      }
    });

    let sort = new ArraySortPipe();

    this.pendingBuildsArray = sort.transform(this.pendingBuildsArray, '');
    this.failedBuildsArray = sort.transform(this.failedBuildsArray, '');
    this.successBuildsArray = sort.transform(this.successBuildsArray, '');

    let index = 1;

    this.pendingBuildsArray.forEach((build) => {
      build.index = index;
      index++
    });
    this.failedBuildsArray.forEach((build) => {
      build.index = index;
      index++
    });
    this.successBuildsArray.forEach((build) => {
      build.index = index;
      index++
    });

    this.cdr.detectChanges();
  }

  adjustToScreenHeight() {
    // Try to set opimal height to show 4 rows on screen
    // 745 Is height of rows 2,3,4 inclusive margines.
    let firstRowHeight = $(window).height() - 805;
    $('.first-row .ablock').height(Math.max(firstRowHeight, 280));
  }

  updatePending(repoSettings: RepoSettings) {
    this.reposService.getPending(this.builds, repoSettings);
  }
}
