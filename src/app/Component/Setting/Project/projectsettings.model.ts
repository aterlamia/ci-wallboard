export class ProjectSettings {
  public enabled: boolean;

  constructor(private name: string, enabled: boolean) {
    this.name = name;
    this.enabled = enabled;
  }

  getName() {
    return this.name;
  }

  isEnabled() {
    return this.enabled;
  }
}
