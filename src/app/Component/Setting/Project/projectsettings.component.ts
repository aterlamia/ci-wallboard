import {Component} from '@angular/core';
import {ProjectSettings} from './projectsettings.model';
import {ProjectRepoSettings} from './projectreposettings.model';
import {Store} from "../../../Service/store";
import {CiRequestFactory} from "../../../Service/ciRequestFactory";
import {RepoSettings} from "../reposettings.model";
import {Toast, ToasterModule, ToasterService} from 'angular2-toaster';

@Component({
  templateUrl: './projectsettings.component.html',
})

export class ProjectSettingsComponent {
  public settings = [];
  private repoSettings: Array<RepoSettings>;

  public showSaved = false;

  private loadedSettings = {};

  constructor(private store: Store, private requestFactory: CiRequestFactory, private toasterService: ToasterService) {
    this.repoSettings = this.store.getRepoSettings();
    this.getProjects();

    let settings = this.store.getValue('project-settings');

    settings.forEach((project) => {
      project.projectSettings.forEach((setting) => {
        this.loadedSettings[project.name + '_' + setting.name] = setting.enabled;
        console.log(setting);
      });
    });

  }

  getProjects() {
    let that = this;
    this.repoSettings.forEach((repoSetting) => {
      if (!this.isEmpty(repoSetting.getName())) {
        console.info('PROJECT SETTINGS COMPONENT: adding project item ' + repoSetting.getKey());
        let request = this.requestFactory.create('travis');
        let resource = request.loadProjectData(repoSetting);

        resource.then(
          project => {
            that.addToSettings(project, repoSetting);
          },
          error => {
            console.error(error);
          }
        );

      }
    }, this);
  }

  isEmpty(val) {
    return (val === "" || typeof(val) === "undefined" || val === null);
  }

  addToSettings(projects, repoSetting: RepoSettings) {
    let repoProjectSettings = new ProjectRepoSettings(repoSetting.getKey());

    projects.forEach((project) => {
      let enabled = this.loadedSettings[repoSetting.getKey() + '_' + project.getSlug()];

      if (typeof(enabled)  === 'undefined') {
        enabled = true;
      }
      repoProjectSettings.addSettings(new ProjectSettings(project.getSlug(), enabled));
    }, this);
    this.settings.push(repoProjectSettings);
  }

  submit() {
    // this.toasterService.pop('success', 'Saved', 'Your Repository was saved');
    let toast: Toast = {
      type: 'success',
      title: 'Saved',
      body: 'Your Project settings were saved',
      showCloseButton: true,
    };

    this.toasterService.pop(toast);
    console.log(JSON.stringify(this.settings));
    this.store.setValue('project-settings', JSON.stringify(this.settings));
  }
}



