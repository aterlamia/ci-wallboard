export class RepoSettings {
  private name: string;
  private token: string;
  private refreshRate: number;
  private lastUpdate: number;
  private _environment: string;
  private _url: string;

  constructor(name: string, token: string, refreshRate: number, environment: string, url: string) {
    this.name = name;
    this.token = token;
    this._environment = environment;
    this._url = url;
    this.refreshRate = refreshRate || 60;
  }

  getEnvironment(): string {
    return this._environment;
  }

  get url(): string {
    return this._url;
  }

  getName(): string {
    return this.name;
  }

  getToken(): string {
    return this.token;
  }

  getKey(): string {
    let postfix = "";
    if (this.isPrivate()) {
      postfix = '_private';
    }
    return this.getName() + postfix;
  }

  public isPrivate(): boolean {
    return typeof(this.token) !== "undefined" && this.token != "" && this.token != null;
  }

  shouldUpdate(interval): boolean {
    if (typeof this.lastUpdate === "undefined" || this.lastUpdate >= (this.refreshRate * interval)) {
      this.lastUpdate = 0;
      return true;
    } else {
      this.lastUpdate += interval;
      return false;
    }
  }
}
